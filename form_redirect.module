<?php
/**
 * @file
 *  Add entity form redirect paths that trigger on when form is saved.
 */

/**
 * Implements hook_permission().
 */
function form_redirect_permission() {
  $permissions['administer entity form redirects'] = array(
    'title' => t('Administer entity form redirects'),
    'description' => t("Administer redirects on form submission."),
  );
  return $permissions;
}

/**
 * Implements hook_menu().
 */
function form_redirect_menu() {
  $items['admin/search/config/form-redirect'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('form_redirect_settings_form'),
    'access arguments' => array('administer entity form redirects'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Define form redirect settings page.
 *
 * @param $form
 *  Form array.
 * @param $form_state
 *  Form state array.
 * @return mixed
 */
function form_redirect_settings_form($form, &$form_state) {

  // Set options from existing content types.
  $content_types = node_type_get_types();
  $form['node'] = array(
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#title' => t('Add redirect paths for content type and user forms. Leave blank for no redirect.'),
    '#description' => t('You can enter internal paths like "node/123" or external "http://example.com/".'),
  );

  // Add redirect input fields for existing node content types.
  foreach ($content_types as $content_type) {
    if ($content_type->module == 'node') {
      $form['node'][$content_type->type . '_submit_redirect'] = array(
        '#type' => 'textfield',
        '#title' => t($content_type->name),
        '#default_value' => variable_get($content_type->type . '_submit_redirect'),
        '#size' => 60,
        '#maxlength' => 128,
      );
    }
  }

  // Add another input for user form.
  $form['node']['user_submit_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#default_value' => variable_get('user_submit_redirect'),
    '#size' => 60,
    '#maxlength' => 128,
  );

  return system_settings_form($form);
}

/**
 *  Implements hook_form_alter().
 */
function form_redirect_form_alter(&$form, &$form_state, $form_id) {
  // Defend against unsupported forms.
  $entity_type = isset($form['#entity_type']) ? $form['#entity_type'] : NULL;
  if ($entity_type != 'node' && $entity_type != 'user') {
    return NULL;
  }

  // Add submit handler and store redirect path.
  $redirect_path = form_redirect_get_node_redirect_path($form['#bundle']);
  if ($redirect_path) {
    $form['actions']['submit']['#submit'][] = 'form_redirect_form_submit';
    $form_state['redirect_path'] = $redirect_path;
  }
}

/**
 * Get redirect path for form type.
 */
function form_redirect_get_node_redirect_path($bundle) {
  $path = variable_get($bundle . '_submit_redirect');

  // Make sure path exists and it's accessible.
  if ($path && drupal_valid_path($path)) {
    return $path;
  }

  return NULL;
}

/**
 * Define submit adding redirects after form submission.
 *
 * @param $form
 *  Form array.
 * @param $form_state
 *  Form state array.
 */
function form_redirect_form_submit($form, &$form_state) {
  if (isset($form_state['redirect_path'])) {
    $form_state['redirect'] = $form_state['redirect_path'];
  }
}